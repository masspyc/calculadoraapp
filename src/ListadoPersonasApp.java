import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ListadoPersonasApp {
    public static void main(String[] args) {
        Scanner consola = new Scanner(System.in);
        // Definimos la lista fuera del ciclo while para que no se borren los datos
        List <Persona> personas = new ArrayList<>();
        // Empezamos con el menu
        var salir = false;
        while (!salir){
            mostrarMenu();
            try {
                salir = ejecutarOperacion(consola, personas);
            } catch (Exception e){
                System.out.println("Ocurrió un error: " + e.getMessage());
            }
            System.out.println("");
        }
    }

    private static void mostrarMenu(){
        //Mostramos las opciones
        System.out.println("""
                ****Listado Personas App****
                1. Agregar
                2. Listar
                3. Salir
                """);
        System.out.print("Proporciona la Opción: ");
    }

    private static boolean ejecutarOperacion(Scanner consola, List <Persona> personas){
        //var opcion = Integer.parseInt(consola.nextLine());
        var opcion = consola.nextInt();
        var salir = false;
        switch (opcion){
            case 1-> { //Agregar una persona a la lista
                System.out.print("Proporciona el nombre de la persona: ");
                var nombre = consola.nextLine();
                System.out.print("Proporciona el teléfono de la persona: ");
                var telefono = consola.nextLine();
                System.out.print("Proporciona el email de la persona: ");
                var email = consola.nextLine();
                //Creamos el objeto Persona
                var persona = new Persona(nombre, telefono, email);
                //Lo agregamos a la lista
                personas.add(persona);
                System.out.println("La lista tiene: " + personas.size() + " elementos");
            }
            case 2-> { //Listado de las personas en la lista
                System.out.println("Listado de personas en la Lista");
                //Mejora usando lambda y métodos de referencia
                //personas.forEach((persona) -> System.out.println(persona)); //lambda
                personas.forEach(System.out::println); //métodos de referencia
            }
            case 3-> { // Salimos de la Aplicación
                System.out.println("Gracias por utilizar la App de Listado de Personas");
                salir = true;
            }
            default->
                System.out.println("Opción Erronea " + opcion);
        }
        return salir;
    }
}